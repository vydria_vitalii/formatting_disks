package ua.khpi.vydria_vitalii.model;


/**
 * Расчет объема диска.
 */
public class CalcSizeDisk {

    /**
     * Метод расчета объема диска (из байтов в Gb).
     *
     * @param size - размер диска в байтах.
     * @return полчения размера диска в Gb.
     */
    public static String toGb(Long size) {
        return String.valueOf(size / (1024/*Kb*/ * 1024/*Mb*/ * 1024/*Gb*/) + " Gb");
    }

    /**
     * Метод расчета объема диска (из Float в Gb).
     *
     * @param size - размер диска в формате Float
     * @return получения размера диска в Gb.
     */
    public static String toGbFloat(Float size) {
        return String.format("%.2f", size / (1024/*Kb*/ * 1024/*Mb*/ * 1024/*Gb*/)) + " Gb";
    }
}
