package ua.khpi.vydria_vitalii.model;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;


/**
 * Окно представления лога процесса форматироания.
 */
public class LogWindow extends AlertWindwow {
    private Label label; ///< надпись.
    private TextArea textArea;  ///< контент
    private final int HEIGHT = 450; ///< высота окна.
    private final int WIDTH = 450; ///< ширина окна.

    /**
     * Конструктор с параметрами.
     *
     * @param title       - название окна.
     * @param headerText  - текст щапки окна.
     * @param contentText - текст контента.
     * @param labelText   - текст надписи.
     */
    public LogWindow(String title, String headerText, String contentText, String labelText) {
        super(Alert.AlertType.INFORMATION, title, headerText, contentText);
        this.label = new Label(labelText);
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane expContent = new GridPane();
        expContent.add(this.label, 0, 0);
        expContent.add(textArea, 0, 1);
        expContent.setMaxWidth(HEIGHT);
        expContent.setMaxHeight(WIDTH);
        getAlert().getDialogPane().setMaxHeight(HEIGHT);
        getAlert().getDialogPane().setMaxWidth(WIDTH);
        getAlert().getDialogPane().setContent(expContent);
        getAlert().getDialogPane().lookupButton(ButtonType.OK).setDisable(true);
    }

    /**
     * Метод установки подписи.
     *
     * @param textLabel - назвние подписи.
     */
    public final void setTextLable(final String textLabel) {
        label.setText(textLabel);
    }

    /**
     * Установка контента окна.
     *
     * @param textArea - текст контента.
     */
    public final void setTextArea(final String textArea) {
        this.textArea.setText(textArea);
    }

    /**
     * Получение текста подписи.
     *
     * @return текст подписи.
     */
    public final String getTextLabel() {
        return label.getText();
    }

    /**
     * Получения контента.
     *
     * @return констент.
     */
    public final String getContentTextArea() {
        return textArea.getText();
    }

    /**
     * Добавление текст в конец лога.
     *
     * @param text - текст.
     */
    public final void addTextArea(final String text) {
        textArea.appendText(text);
    }
}
