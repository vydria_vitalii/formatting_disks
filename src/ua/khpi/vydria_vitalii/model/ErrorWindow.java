package ua.khpi.vydria_vitalii.model;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;


/**
 * Диалоговое окно ошибок.
 */
public class ErrorWindow extends AlertWindwow {
    private final static int MAX_HEIGHT = 600; ///< максимальная высокта окна.
    private final static int MAX_WIDTH = 600;///< максимальная ширина окна.
    private Label label; ///< надпись.
    private TextArea textArea; ///< контент
    private StringWriter sw; ///< поток считывания.
    private PrintWriter pw; ///< поток записи.

    /**
     * Конструктор с параметрами.
     *
     * @param title       - название окна.
     * @param headerText  - текст щапки окна.
     * @param contentText - текст контенат окна.
     * @param labelText   - подпись.
     * @param ex          - Exception.
     */
    public ErrorWindow(String title, String headerText, String contentText, String labelText, Exception ex) {
        super(Alert.AlertType.ERROR, title, headerText, contentText);
        sw = new StringWriter();
        pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        this.label = new Label(labelText);
        textArea = new TextArea(sw.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setMaxWidth(MAX_WIDTH);
        textArea.setMaxHeight(MAX_HEIGHT);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);
        GridPane expContent = new GridPane();
        expContent.setMaxWidth(MAX_WIDTH);
        expContent.setMaxHeight(MAX_HEIGHT);
        expContent.add(this.label, 0, 0);
        expContent.add(textArea, 0, 1);
        getAlert().getDialogPane().setExpandableContent(expContent);
        getAlert().getDialogPane().setMaxHeight(MAX_HEIGHT);
        getAlert().getDialogPane().setMaxWidth(MAX_WIDTH);
        getAlert().getDialogPane().getScene().heightProperty().addListener((observableValue, oldSceneHeight, newSceneHeight) -> {
            if (newSceneHeight.doubleValue() > MAX_HEIGHT) {
                getAlert().setHeight(MAX_HEIGHT);
            }
        });
        getAlert().getDialogPane().getScene().widthProperty().addListener((observable, oldWidthValue, newWidthValue) -> {
            if (newWidthValue.doubleValue() > MAX_WIDTH) getAlert().setWidth(MAX_WIDTH);
        });
    }

    /**
     * Метод установки подписи.
     *
     * @param str - назвние подписи.
     */
    public final void setLable(final String str) {
        label.setText(str);
    }

    /**
     * Метод установиек контента.
     *
     * @param e - Exception.
     */
    public final void setContentError(final Exception e) {
        e.printStackTrace(pw);
        textArea.setText(sw.toString());
    }

    /**
     * Установка контента окна.
     *
     * @param str - текст контента.
     */
    public final void setContentError(final String str) {
        textArea.setText(str);
    }
}
