package ua.khpi.vydria_vitalii.model;

import ua.khpi.vydria_vitalii.Main;


/**
 * Формирование команды, для отправки в консоль.
 */
public class BuildCommandFormatting {
    private final String disk; ///< имя диска.
    private final String fileSystem; ///< файловая система.
    private final String sizeClaster; ///< размер кластера
    private final boolean fast; ///< флаг быстрого форматированеи.
    private final static String QUICKLY_ATR = "/q"; ///< атрибут быстрого форматирования.

    /**
     * Конструктор с параметрами.
     *
     * @param disk        - имя диска.
     * @param fileSystem  - файловая система.
     * @param sizeClaster - размер кластера.
     * @param fast        - флаг быстрого форматирования.
     */
    public BuildCommandFormatting(String disk, String fileSystem, String sizeClaster, boolean fast) {
        this.disk = (disk + ":");
        this.fileSystem = fileSystem;
        if (Main.getCLASTER()[Main.getIndexDefaulClaster()][Main.getIndexDefaulClaster()] != sizeClaster) {
            StringBuilder tmp = new StringBuilder("/a:" + sizeClaster.substring(0, sizeClaster.indexOf(" ")));
            if (sizeClaster.lastIndexOf("К") != -1) {
                tmp.append("K");
            }
            this.sizeClaster = tmp.toString();
        } else {
            this.sizeClaster = "";
        }
        this.fast = fast;
    }

    /**
     * Получения имени диска.
     *
     * @return имя диска.
     */
    public String getDisk() {
        return disk;
    }

    /**
     * Получения файловой системы.
     *
     * @return файловая система.
     */
    public String getFileSystem() {
        return fileSystem;
    }

    /**
     * Получения флага быстрого форматирования.
     *
     * @return флаг быстрого форматирования.
     */
    public boolean isFast() {
        return fast;
    }

    /**
     * Получения размера кластера.
     *
     * @return размер кластера.
     */
    public String getSizeClaster() {
        return sizeClaster;
    }

    /**
     * Получения команды форматирования.
     *
     * @return команда форматированя.
     */
    public String getFormattingCommand() {
        StringBuilder command = new StringBuilder();
        command.append("format " + disk + " /fs:" + fileSystem);
        if (fast) {
            command.append(QUICKLY_ATR);
        }
        command.append(sizeClaster + "/y");
        return command.toString();
    }

    /**
     * Перегрузка toString.
     *
     * @return команда форматированя.
     */
    @Override
    public String toString() {
        return getFormattingCommand();
    }
}
