package ua.khpi.vydria_vitalii.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Пресдстваление файловой системы.
 */
public class FileSystem {
    private final String nameFileSystem; ///< название файловой системы.
    private ObservableList<String> sizeClaster; ///< коллекуия размеров кластеров файловой системы

    {
        sizeClaster = FXCollections.observableArrayList();
    }

    /**
     * Получения списка размеров кластеров файлвой системы.
     *
     * @return коллекция размеров кластеров файловой сиситемы.
     */
    public ObservableList<String> getSizeClaster() {
        return sizeClaster;
    }

    /**
     * Переопределение toString.
     *
     * @return название файловой системы.
     */
    @Override
    public String toString() {
        return getNameFileSystem();

    }

    /**
     * Получение названия файловой системы.
     *
     * @return название файловой системы.
     */
    public String getNameFileSystem() {
        return nameFileSystem;
    }

    /**
     * Конструктор с параметрами.
     *
     * @param nameFileSystem - название файловой системы.
     * @param str            - список размеров кластеров файловой системы.
     */
    public FileSystem(String nameFileSystem, String... str) {
        this.nameFileSystem = nameFileSystem;
        for (String item : str) {
            sizeClaster.add(item);
        }
    }
}
