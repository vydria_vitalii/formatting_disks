package ua.khpi.vydria_vitalii.model;

/**
 * Интерфейс очереди.
 */
public interface Queue {
    /**
     * Добавление задачи в очередь.
     *
     * @param cmd - задача.
     */
    void put(Command cmd);

    /**
     * Получение первой пришедшей задачи в очередь.
     *
     * @return задучу.
     */
    Command take();
}

