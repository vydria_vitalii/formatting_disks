package ua.khpi.vydria_vitalii.model;

import java.util.concurrent.TimeUnit;

/**
 * Команда обновления контента окна лога форматирования.
 */
public class CommandUpdateLog implements Command {
    private Window win; ///< ссылка на производный класс интерфейса Window.
    private String buff; ///< буфер сообщения.

    /**
     * Конструктор по умолчанию.
     *
     * @param win  - ссылка на производный класс интерфейса Window.
     * @param buff - буфер.
     */
    public CommandUpdateLog(Window win, String buff) {
        this.win = win;
        this.buff = buff;
    }

    /**
     * Метод выполнения команды.
     */
    @Override
    public void execute() {
        try {
            TimeUnit.MILLISECONDS.sleep(100);
            ((LogWindow) win).addTextArea(buff + "\n");
        } catch (Exception ex) {
            ErrorWindow errorWindow = new ErrorWindow("Exception Dialog", "Look, an Exception Dialog",
                    "Error formatting Disk", "The exception stacktrace was:", ex);
            errorWindow.showAndWait();
            System.exit(-1);
        }
    }
}
