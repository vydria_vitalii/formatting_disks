package ua.khpi.vydria_vitalii.model;

/**
 * Интерфейс сущьности окна.
 */
public interface Window {
    /**
     * Отображение окна.
     */
    void show();

    /**
     * Отображение окна и ожидание его завершения.
     */
    void showAndWait();
}
