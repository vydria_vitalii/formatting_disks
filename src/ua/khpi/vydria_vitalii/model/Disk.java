package ua.khpi.vydria_vitalii.model;


/**
 * Представляет сущность локального диска.
 */
public class Disk {
    private final String name;  ///< имя диска
    private final Long totalSpace; ///< полный объем диска
    private final Long freeSpace; ///< свободно на диске места
    private final Long usableSpace; ///< занято на диске места

    /**
     * Получения объема диска.
     *
     * @return объем диска.
     */
    public Long getTotalSpace() {
        return totalSpace;
    }


    /**
     * Конструктор копирования.
     *
     * @param item - объект класса Disk.
     */
    public Disk(Disk item) {
        name = item.getName();
        totalSpace = new Long(item.getTotalSpace());
        freeSpace = new Long(item.getFreeSpace());
        usableSpace = new Long(item.getUsableSpace());
    }

    /**
     * Получение свободного пространства на диске.
     *
     * @return свободное пространство на диске.
     */
    public Long getFreeSpace() {
        return freeSpace;
    }

    /**
     * Получение занятого пространства на диске.
     *
     * @return занятое пространсво.
     */
    public Long getUsableSpace() {
        return usableSpace;
    }

    /**
     * Полусение имени диска.
     *
     * @return имя диска.
     */
    public String getName() {
        return name;
    }

    /**
     * Конструктор с параметрами.
     *
     * @param name        - имя диска.
     * @param totalSpace  - объем диска.
     * @param freeSpace   - свободное пространство.
     * @param usableSpace - занятое пространство.
     */
    public Disk(String name, Long totalSpace, Long freeSpace, Long usableSpace) {
        this.name = name;
        this.totalSpace = totalSpace;
        this.freeSpace = freeSpace;
        this.usableSpace = usableSpace;
    }

    /**
     * Перегрузка toString.
     *
     * @return имя диска.
     */
    @Override
    public String toString() {
        return name;
    }
}
