package ua.khpi.vydria_vitalii.model;

import java.util.Vector;

/**
 * Создает обработчик потока, выполняющего объекты с интерфейсом Command;
 * Шаблое Worker Thread.
 */
public class CommandQueue implements Queue {
    private Vector<Command> tasks = null; ///< очередь задач.
    private boolean waiting; ///< флаг ожидания.
    private boolean shutdown; ///< флаг завершения.

    /**
     * Получения очереди задач.
     *
     * @return объект очереди задач.
     */
    public Vector<Command> getTasks() {
        return tasks;
    }

    public CommandQueue() {
        this.tasks = new Vector<>();
        this.waiting = false;
        shutdown = false;
        new Thread(new Worker()).start();
    }

    /**
     * Задача, используемая обработчиком потока;
     */
    public void shutdown() {
        shutdown = true;
    }


    /**
     * Добавление задачи в очередь.
     *
     * @param cmd - задача.
     */
    @Override
    public void put(Command cmd) {
        tasks.add(cmd);
        if (waiting) {
            synchronized (this) {
                notifyAll();
            }
        }
    }

    /**
     * Получение первой пришедшей задачи в очередь.
     *
     * @return задучу.
     */
    @Override
    public Command take() {
        if (tasks.isEmpty()) {
            synchronized (this) {
                waiting = true;
                try {
                    wait();
                } catch (InterruptedException e) {
                    waiting = false;
                }
            }
        }
        return tasks.remove(0);
    }

    /**
     * Внутренний класс задач.
     * Шаблое Worker Thread.
     */
    private class Worker implements Runnable {
        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            while (!shutdown) {
                Command r = take();
                r.execute();
            }
        }
    }
}

