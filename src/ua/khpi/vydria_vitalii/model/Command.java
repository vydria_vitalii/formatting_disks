package ua.khpi.vydria_vitalii.model;

/**
 * Интерфейс команд.
 */
public interface Command {

    /**
     * Метод выполнения команды.
     */
    void execute();
}
