package ua.khpi.vydria_vitalii.model;

import javafx.scene.control.ButtonType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;


/**
 * Команда форматирования диска.
 */
public class CommandFormatting implements Command {
    private static final String CMD = "cmd.exe"; ///< консоль.
    private BuildCommandFormatting buildCommandFormatting; ///< объект построения команды.
    private Window win; ///< ссылка на производный класс интерфейса Window.
    private CommandQueue queue = new CommandQueue(); ///< объект класса CommandQueue.

    /**
     * Конструктор с параметрами.
     *
     * @param buildCommandFormatting - объект класса BuildCommandFormatting.
     * @param win                    - ссылка на производный класс интерфейса Window.
     */
    public CommandFormatting(BuildCommandFormatting buildCommandFormatting, Window win) {
        this.buildCommandFormatting = buildCommandFormatting;
        this.win = win;
    }

    /**
     * Метод установки объекта BuildCommandFormatting.
     *
     * @param buildCommandFormatting - объект класса BuildCommandFormatting.
     */
    public void setBuildCommandFormatting(BuildCommandFormatting buildCommandFormatting) {
        this.buildCommandFormatting = buildCommandFormatting;
    }

    /**
     * Метод выполнения команды.
     */
    @Override
    public void execute() {
        try {
            ProcessBuilder builder = new ProcessBuilder(CMD, "/c", buildCommandFormatting.getFormattingCommand());
            builder.redirectErrorStream(true);
            Process p = builder.start();
            InputStreamReader buf = new InputStreamReader(p.getInputStream());
            BufferedReader r = new BufferedReader(buf);
            String line = null;
            while (true) {
                line = r.readLine();
                if (line == null) {
                    break;
                }
                queue.put(new CommandUpdateLog(win, line));
                TimeUnit.MILLISECONDS.sleep(line.length() / 500);
            }
            TimeUnit.MILLISECONDS.sleep(1);
            ((LogWindow) win).getAlert().getDialogPane().lookupButton(ButtonType.OK).setDisable(false);
            queue.shutdown();
        } catch (Exception ex) {
            ErrorWindow errorWindow = new ErrorWindow("Exception Dialog", "Look, an Exception Dialog",
                    "Error formatting Disk", "The exception stacktrace was:", ex);
            errorWindow.showAndWait();
            System.exit(-1);
        }
    }
}
