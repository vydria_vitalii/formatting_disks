package ua.khpi.vydria_vitalii.model;

import javafx.scene.control.Alert;

/**
 * Диалоговое окно сообщения.
 */
public class AlertWindwow implements Window {
    private Alert alert; ///< диалоговое окно.
    private static final Alert.AlertType TYPE_ALERT = Alert.AlertType.NONE; ///< стиль окна по умолчанию.

    /**
     * Конструктор с параметрами.
     *
     * @param title       - назвние окна.
     * @param headerText  - текст заголовка.
     * @param contentText - текст контента.
     */
    public AlertWindwow(String title, String headerText, String contentText) {
        this(TYPE_ALERT, title, headerText, contentText);
    }

    /**
     * Конструктор по умолчанию.
     */
    public AlertWindwow() {
        this(Alert.AlertType.NONE);
    }

    /**
     * Коснтруктор с параметрами.
     *
     * @param type - тип окна.
     */
    public AlertWindwow(Alert.AlertType type) {
        alert = new Alert(type);
    }

    /**
     * Констуктор с параметрами.
     * @param type - тип окна.
     * @param title - название окна.
     * @param headerText - текст шапки.
     * @param contentText - контент.
     */
    public AlertWindwow(Alert.AlertType type, String title, String headerText, String contentText) {
        alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
    }

    /**
     * Установка имени окна.
     * @param title имя окна
     */
    public final void setTitle(final String title) {
        alert.setTitle(title);
    }

    /**
     * Установка шапки.
     * @param heeaderText - текст шапки.
     */
    public final void setHeaderText(final String heeaderText) {
        alert.setHeaderText(heeaderText);
    }

    /**
     * Установка контента.
     * @param contentText - контент.
     */
    public final void setContentText(final String contentText) {
        alert.setContentText(contentText);
    }

    /**
     * Метод получения объекта Alert.
     * @return объект класса Alert.
     */
    public final Alert getAlert() {
        return alert;
    }

    /**
     * Показать.
     */
    @Override
    public void show() {
        alert.show();
    }

    /**
     * Показать с ожиданием.
     */
    @Override
    public void showAndWait() {
        alert.showAndWait();
    }
}
