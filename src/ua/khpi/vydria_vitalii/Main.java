package ua.khpi.vydria_vitalii;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ua.khpi.vydria_vitalii.model.Disk;
import ua.khpi.vydria_vitalii.model.FileSystem;
import ua.khpi.vydria_vitalii.view.DiskOverviewController;

import java.io.File;

/**
 * Главный класс.
 */
public class Main extends Application {
    private static final double MIN_HEIGHT = 398; ///< минимальная высота окна.
    private static final double MIN_WIDHT = 350; ///< минимальная шириина окна.
    private static final double MAX_HEIGHT = 500; ///< максимальная высота окна.
    private static final double MAX_WIDHT = 450; ///< максимальная ширина окна.
    private static final int INDEX_DEFAUL_CLASTER = 0; ///< индекс кластера по умолчанию.

    /**
     * Массив размеров кластеров.
     */
    private static final String CLASTER[][] = new String[][]{
            {"Стандартный размер кластера", "512 байт", "1024 байт", "2048 байт", "4096 байт", "8192 байт", "16 КБ", "32 КБ", "64 КБ"},
            {"Стандартный размер кластера", "8192 байт", "16 КБ", "32 КБ", "64 КБ"}
    };

    /**
     * Метод получения массива файловых систем.
     *
     * @return массив файловых систем.
     */
    public static String[] getFileSystem() {
        return FILE_SYSTEM;
    }

    /**
     * Получение масства класстеров.
     *
     * @return массив размеров класстеров.
     */
    public static String[][] getCLASTER() {
        return CLASTER;
    }

    /**
     * Получения индекса класстера по умолчанию.
     *
     * @return индекс кластера по умолчанию.
     */
    public static int getIndexDefaulClaster() {
        return INDEX_DEFAUL_CLASTER;
    }

    private static final String FILE_SYSTEM[] = {"NTFS", "FAT32"}; ///< массив файловых систем.

    /**
     * Получение объекта коллекции  ObservableList<Disk>.
     *
     * @return коллекция ObservableList<Disk>.
     */
    public ObservableList<Disk> getDisks() {
        return disks;
    }

    public ObservableList<FileSystem> getFileSystems() {
        return fileSystems;
    }

    private ObservableList<Disk> disks = FXCollections.observableArrayList();
    private ObservableList<FileSystem> fileSystems = FXCollections.observableArrayList();

    /**
     * Обновление списка дисков.
     */
    public void updateDisk() {
        for (File f : File.listRoots()) {
            disks.add(new Disk(f.toString().substring(0, f.toString().length() - 2), new Long(f.getTotalSpace()), new Long(f.getFreeSpace()), new Long(f.getTotalSpace() - f.getFreeSpace())));
        }
    }

    /**
     * Конструктор по умолчанию.
     */
    public Main() {
        for (int i = 0; i < FILE_SYSTEM.length || i < CLASTER.length; i++) {
            fileSystems.add(new FileSystem(FILE_SYSTEM[i], CLASTER[i]));
        }
        updateDisk();
    }

    /**
     * Запуск окна.
     *
     * @param primaryStage - объект класса Stage.
     * @throws Exception - базовое исключение.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("view/DiskOverview.fxml"));
        primaryStage.setTitle("Формотирование дисков");
        primaryStage.setScene(new Scene(loader.load(), 400, 400));
        primaryStage.show();
        primaryStage.setMinHeight(MIN_HEIGHT);
        primaryStage.setMinWidth(MIN_WIDHT);
        primaryStage.setMaxHeight(MAX_HEIGHT);
        primaryStage.setMaxWidth(MAX_WIDHT);
        DiskOverviewController controller = loader.getController();
        controller.setMain(this);
    }

    /**
     * Главный метод.
     *
     * @param args - аргументы метода.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
