package ua.khpi.vydria_vitalii.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ua.khpi.vydria_vitalii.Main;
import ua.khpi.vydria_vitalii.model.*;

import java.util.Optional;

/**
 * Created by Vitaliy on 07.12.2015.
 */
public class DiskOverviewController {

    @FXML
    public TextField used; ///< элемент окна TextField.
    @FXML
    public TextField empty; ///< элемент окна TextField.
    @FXML
    public ComboBox<Disk> disk; ///< элемент окна ComboBox.
    @FXML
    public MenuItem menuExit; ///< элемент окна MenuItem.
    @FXML
    public AnchorPane window; ///< элемент окна AnchorPane.
    @FXML
    public ComboBox<FileSystem> system; ///< элемент окна ComboBox.
    @FXML
    public ComboBox<String> claster; ///< элемент окна ComboBox.
    @FXML
    public CheckBox quickly; ///< элемент окна CheckBox.
    @FXML
    private TextField size; ///< элемент окна TextField.
    private Main main; ///< объект класса Main.

    /**
     * Инициализация окна.
     */
    @FXML
    private void initialize() {
        disk.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showDiskDetails(newValue));
        system.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showFileSystemDetails(newValue));
    }


    /**
     * Отображение деталей диска...
     *
     * @param aDisk - объек класса Disk.
     */
    public void showDiskDetails(Disk aDisk) {
        if (aDisk != null) {
            size.setText(CalcSizeDisk.toGbFloat(Float.parseFloat(String.valueOf(aDisk.getTotalSpace()))));
            used.setText(CalcSizeDisk.toGbFloat(Float.parseFloat(String.valueOf(aDisk.getUsableSpace()))));
            empty.setText(CalcSizeDisk.toGbFloat(Float.parseFloat(String.valueOf(aDisk.getFreeSpace()))));
        } else {
            size.setText("");
            used.setText("");
            empty.setText("");
        }
    }

    /**
     * Отобра деталей файловой системы.
     *
     * @param aFileSystem - объект класса FileSystem.
     */
    public void showFileSystemDetails(FileSystem aFileSystem) {
        claster.setItems(aFileSystem.getSizeClaster());
        claster.getSelectionModel().selectFirst();
    }

    /**
     * Выход из прогрыммы.
     *
     * @param actionEvent - событие.
     */
    @FXML
    public void clickExit(ActionEvent actionEvent) {
        ((Stage) window.getScene().getWindow()).close();
    }

    /**
     * Отображение информации об авторе.
     *
     * @param actionEvent - событие.
     */
    @FXML
    public void showAboutMe(ActionEvent actionEvent) {
        AlertWindwow win = new AlertWindwow(Alert.AlertType.INFORMATION);
        win.setTitle("Обо мне !");
        win.setHeaderText(null);
        win.setContentText("Работу выполнил ст. гр. КИТ-23А Выдря Виталий Юрьевич");
        win.showAndWait();
    }

    /**
     * Установка объекта класса Main.
     *
     * @param main -  объект класса Main.
     */
    public void setMain(Main main) {
        this.main = main;
        disk.setItems(main.getDisks());
        disk.getSelectionModel().selectFirst();
        system.setItems(main.getFileSystems());
        system.getSelectionModel().selectFirst();
    }

    /**
     * Обновление списков диска.
     */
    @FXML
    public void updateControllerDisk() {
        main.getDisks().clear();
        main.updateDisk();
        disk.getSelectionModel().selectFirst();
    }


    /**
     * Запус форматиования выбранного диска.
     * @param actionEvent - событие.
     */
    @FXML
    public void runFormatting(ActionEvent actionEvent) {
        try {
            AlertWindwow alert = new AlertWindwow(Alert.AlertType.CONFIRMATION, "Согласие...", "Внимание! Форматирование уничтожит ВСЕ данные на " +
                    "этом диске.", "Нажмите кнопку 'OK' для запуска форматирования, 'Cancel' для его отмены");
            Optional<ButtonType> result = alert.getAlert().showAndWait();
            if (result.get() == ButtonType.OK) {
                LogWindow w = new LogWindow("Форматирование диска", "Процесс форматирования", "Look, an Formatting Dialog", "Log formatting Disk:");
                CommandFormatting formatting = new CommandFormatting(new BuildCommandFormatting(disk.getSelectionModel().getSelectedItem().getName(),
                        system.getSelectionModel().getSelectedItem().getNameFileSystem(), claster.getSelectionModel().getSelectedItem().toString(), quickly.isSelected()), w);
                CommandQueue queue = new CommandQueue();
                queue.put(formatting);
                w.showAndWait();
                queue.shutdown();
                updateControllerDisk();
            }
        } catch (Exception ex) {
            ErrorWindow errorWindow = new ErrorWindow("Exception Dialog", "Look, an Exception Dialog",
                    "Error formatting Disk", "The exception stacktrace was:", ex);
            errorWindow.showAndWait();
            System.exit(-1);
        }
    }
}