package ua.khpi.vydria_vitalii;

import org.junit.BeforeClass;
import org.junit.Test;
import ua.khpi.vydria_vitalii.model.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Тестирование разработанных классов.
 */
public class MainTest {
    private static final String SIZE_TEST = "43904921600"; ///< размер диска в байтах
    private static final String RESULT_GB = "40"; ///< рамер диска в GB
    private static final String RESULT_GBFLOAT = "40,89"; ///< размер диска в GB с плавающей точкой.
    private static List<Disk> disks; ///< коллекция дисков
    private static CommandQueue queue = new CommandQueue(); ///< объект очереди команд.

    static {
        disks = new ArrayList<>();
    }

    /**
     * Выполняется первым.
     */
    @BeforeClass
    public static final void setUpBeforeClass() {
        for (File item : File.listRoots()) {
            disks.add(new Disk(item.toString().substring(0, item.toString().length() - 2), new Long(item.getTotalSpace()),
                    new Long(item.getFreeSpace()), new Long(item.getTotalSpace() - item.getFreeSpace())) {
                @Override
                public String toString() {
                    return "Disk{" +
                            "name='" + getName() + '\'' +
                            ", totalSpace=" + getTotalSpace() +
                            ", freeSpace=" + getFreeSpace() +
                            ", usableSpace=" + getUsableSpace() +
                            '}';
                }
            });
        }
    }

    /**
     * Тест класса расчета обьема диска.
     */
    @Test
    public void testCalcSizeDisk() {
        String res = CalcSizeDisk.toGb(new Long(Long.parseLong(SIZE_TEST)));
        assertEquals(RESULT_GB + " Gb", res);
        assertEquals(RESULT_GBFLOAT + " Gb", CalcSizeDisk.toGbFloat(Float.parseFloat(SIZE_TEST)));
    }


    /**
     * Тест многопоточности.
     */
    @Test
    public void testCommandWorkerThread() {
        StringBuilder strExpected = new StringBuilder();
        StringBuilder strTest = new StringBuilder();

        class TestWorks implements Command {
            private StringBuilder str;
            private Disk disk;

            public TestWorks(StringBuilder str, Disk item) {
                this.str = str;
                disk = item;
            }

            @Override
            public void execute() {
                str.append(disk.toString() + "\n");
                System.out.println(str.toString() + "\n");
            }
        }
        try {
            for (Disk item : disks) {
                strExpected.append(item + "\n");
                queue.put(new TestWorks(strTest, item));
            }
            Thread.sleep(50000 / strExpected.length());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Тест класса построения команды для отправки в консоль.
     */
    @Test
    public void testBuildCommandFormatting() {
        final int indexListDisk = 0;
        final int indexListFileSystem = 0;
        final int indexListClasterI = 0;
        final int indexListClasterJ = 1;
        String str = "format " + disks.get(indexListDisk).getName() + ": /fs:NTFS/q/a:512/y";
        BuildCommandFormatting commandFormatting = new BuildCommandFormatting(disks.get(indexListDisk).getName(),
                Main.getFileSystem()[indexListFileSystem], Main.getCLASTER()[indexListClasterI][indexListClasterJ], true);
        assertEquals(str, commandFormatting.toString());
    }
}
